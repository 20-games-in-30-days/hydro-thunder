extends Marker3D
class_name Float

@export var buoyancy := 50.0

func get_force(water_level: float) -> Vector3:
	# Calculate depth difference between marker and water level
	var depth_difference = self.global_transform.origin.y - water_level

	if depth_difference > 0: 
		# Marker is above water, no additional force required
		return Vector3(0, 0, 0)

	# Calculate force based on depth difference (only when marker is below water)
	var force_magnitude = -buoyancy * depth_difference

	# Return force as a Vector3 (assuming force is applied only in Y direction)
	return Vector3(0, force_magnitude, 0)
