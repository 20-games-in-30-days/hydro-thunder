extends RigidBody3D

@export var water_level := 0.0

@onready var floats: Array[Float] = [$Float, $Float2, $Float3]
@onready var props: Array[Prop] = [$Prop]

# Variable to hold the input vector
var input: Vector2 = Vector2()


func _physics_process(_delta):
	# Read the input vector
	input = Input.get_vector("left", "right", "gas", "brake")
	
	# Apply forces from each float
	for f in floats:
		apply_force_from_float(f)


# Ensure that drag is applied (like water resistance). You can tweak these values as needed
func _integrate_forces(_state):
	pass


# Helper function to apply the force from a float to the rigid body
func apply_force_from_float(f: Float):
	# Get the force from the float
	var force = f.get_force(water_level)
	
	# Apply the force to the boat at the position of the float
	self.apply_force(f.position, force)
