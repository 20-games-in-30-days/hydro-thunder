## Whisk is a 3D mesh editor. It's called Whisk because it isn't as useful as Blender.
extends Node3D

@onready var vertex_editor = $VertexEditor
@onready var mesh_builder = $MeshBuilder
@onready var vertex_label = $"Gui/Panel/Center/VBox/Vertex Label"
@onready var mirror = $Gui/Panel/Center/VBox/Mirror
@onready var build = $Gui/Panel/Center/VBox/HBoxContainer/Build
@onready var move = $Gui/Panel/Center/VBox/HBoxContainer/Move
@onready var origin = $Static/Origin
var mode := VertexEditor.Mode.PLACE
var previous_pos: Vector3
var Log: Logger


func _init():
	Log = Print.get_or_create_logger("Whisk", Print.INFO, Print.VERBOSE)


func _input(event):
	if event.is_action_pressed("escape"):
		vertex_editor.deselect_all()
		mesh_builder.clear_points()
	if event.is_action_pressed("toggle_mode"):
		if mode == VertexEditor.Mode.PLACE:
			move.button_pressed = true
		elif mode == VertexEditor.Mode.MODIFY:
			build.button_pressed = true


func _on_x_pressed():
	mesh_builder.update_meshes()
	if vertex_editor.selected_plane == VertexEditor.Planes.X_PLANE:
		origin.visible = false
		vertex_editor.selected_plane = VertexEditor.Planes.NONE
	else:
		origin.visible = true
		vertex_editor.snap_all_planes(previous_pos)
		vertex_editor.selected_plane = VertexEditor.Planes.X_PLANE


func _on_y_pressed():
	mesh_builder.update_meshes()
	if vertex_editor.selected_plane == VertexEditor.Planes.Y_PLANE:
		origin.visible = false
		vertex_editor.selected_plane = VertexEditor.Planes.NONE
	else:
		origin.visible = true
		vertex_editor.snap_all_planes(previous_pos)
		vertex_editor.selected_plane = VertexEditor.Planes.Y_PLANE


func _on_z_pressed():
	mesh_builder.update_meshes()
	if vertex_editor.selected_plane == VertexEditor.Planes.Z_PLANE:
		origin.visible = false
		vertex_editor.selected_plane = VertexEditor.Planes.NONE
	else:
		origin.visible = true
		vertex_editor.snap_all_planes(previous_pos)
		vertex_editor.selected_plane = VertexEditor.Planes.Z_PLANE


func _on_none_pressed():
	if vertex_editor.selected_plane != VertexEditor.Planes.NONE:
		origin.visible = true
	else:
		origin.visible = !origin.visible
	mesh_builder.update_meshes()
	vertex_editor.selected_plane = VertexEditor.Planes.NONE


func _on_reset_pressed():
	vertex_editor.snap_all_planes(Vector3.ZERO)


func _on_vertex_editor_point_moved(v: Vertex):
	if v.position != previous_pos:
		previous_pos = v.position
		vertex_label.text = "Vertex: " + str(v.position)
		mesh_builder.update_meshes(v)


func _on_vertex_editor_point_clicked(vertex):
	mesh_builder.add_point(vertex)


func _on_vertex_editor_point_removed():
	mesh_builder.remove_deleted()


func _on_vertex_editor_action_cancelled():
	mesh_builder.clear_points()


func _on_vertex_editor_point_hidden():
	previous_pos = Vector3.ZERO
	mesh_builder.update_meshes()


func _on_mirror_toggled(toggled_on):
	mesh_builder.mirrored = toggled_on
	if toggled_on:
		mirror.text = "Mirror On"
	else:
		mirror.text = "Mirror Off"
	Log.debug(mirror.text)


func _on_build_toggled(toggled_on):
	if toggled_on:
		Log.debug("Switched to PLACEMENT mode.")
		vertex_editor.place_mode = VertexEditor.Mode.PLACE
		mode = VertexEditor.Mode.PLACE


func _on_move_toggled(toggled_on):
	if toggled_on:
		# Turn planes back on for later.
		if vertex_editor.selected_plane == VertexEditor.Planes.NONE:
			vertex_editor.selected_plane = VertexEditor.Planes.X_PLANE
		vertex_editor.deselect_all()
		mesh_builder.clear_points()
		Log.debug("Switched to MODIFY mode.")
		vertex_editor.place_mode = VertexEditor.Mode.MODIFY
		mode = VertexEditor.Mode.MODIFY


func _on_color_picker_button_color_changed(color, mat_num):
	mesh_builder.on_color_changed(color, mat_num)


func _on_material_selected(mat_num):
	mesh_builder.on_material_select(mat_num)
