extends Node3D

var Log: Logger
@onready var fiber_1 = $Fiber1
@onready var fiber_2 = $Fiber2
@onready var fiber_3 = $Fiber3
@onready var chrome = $Chrome
@onready var glass = $Glass

enum Mat {
	FIBERGLASS1,
	FIBERGLASS2,
	FIBERGLASS3,
	CHROME,
	GLASS
}

@export_enum("Fiber1", "Fiber2", "Fiber3", "Chrome", "Glass") var active_material: int = Mat.FIBERGLASS1
@export var mirrored := true

@onready var meshes = {
	Mat.FIBERGLASS1: fiber_1,
	Mat.FIBERGLASS2: fiber_2,
	Mat.FIBERGLASS3: fiber_3,
	Mat.CHROME: chrome,
	Mat.GLASS: glass,
}

var surfaces = {}
var tris = {}

var builder_points: Array[Vertex] = []


## Stores the three points in a single triangle. Also indicates if this should be mirrored across the XY plane.
class Triangle:
	var point1: Vertex
	var point2: Vertex
	var point3: Vertex
	var mirrored := true
	var points: Array[Vector3]:
		get:
			var p: Array[Vector3] = [point1.grid_position, point2.grid_position, point3.grid_position]
			if mirrored:
				p += [point3.mirrored_position, point2.mirrored_position, point1.mirrored_position]
			return p
	var is_invalid: bool:
		get:
			return point1.is_queued_for_deletion() or point2.is_queued_for_deletion() or point3.is_queued_for_deletion()
	
	func _init(p1, p2, p3, is_mirrored):
		point1 = p1
		point2 = p2
		point3 = p3
		mirrored = is_mirrored


## Set up the surface tools.
func _ready():
	Log = Print.get_logger("Whisk", true)
	for m in Mat.values():
		surfaces[m] = SurfaceTool.new()
		tris[m] = []


## Add a point to the triangle that we are actively building.
func add_point(point: Vertex):
	builder_points.append(point)
	Log.debug("Added point to array!")
	if builder_points.size() >= 3:
		_build_triangle()
		Log.debug("Made a triangle from the points!")
		update_meshes()


## Remove all tracked points from the current triangle.
func clear_points():
	builder_points.clear()
	update_meshes()


## Remove all points and triangles from the mesh.
func clear_all():
	builder_points.clear()
	for t in tris:
		t.clear()
	update_meshes()


## Remove all triangles that have vertices pending deletion.
func remove_deleted():
	for material in Mat.values():
		var valid_tris = []
		for triangle in tris[material]:
			if not triangle.is_invalid:
				Log.verbose("Valid triangle, keeping.")
				valid_tris.append(triangle)
			else:
				Log.verbose("Invalid triangle, discarding.")
		tris[material] = valid_tris

	# Remove invalid vertices from builder_points.
	var valid_points: Array[Vertex] = []
	for point in builder_points:
		if not point.is_queued_for_deletion():
			valid_points.append(point)
	builder_points = valid_points
	if valid_points.size() >= 1:
		valid_points[-1].activate()
	if valid_points.size() >= 2:
		valid_points[-2].activate()
	update_meshes()


## Save this triangle to the list of built tris.
func _build_triangle():
	# We finished the triangle.
	tris[active_material].append(Triangle.new(builder_points[-3], builder_points[-2], builder_points[-1], mirrored))
	# Stop highlighting this vertex, it is no longer a part of the active triangle.
	builder_points[-3].deactivate()


## Rebuild the meshes to keep up with the points moving on screen.
func update_meshes(v: Vertex = null):
	for m in Mat.values():
		var points = []
		
		for t in tris[m]:
			points += t.points
		
		# Draw only if we have all three points.
		if active_material == m:
			if v != null and builder_points.size() >= 2:
				points += [builder_points[-2].grid_position, builder_points[-1].grid_position, v.grid_position]
				if mirrored:
					points += [builder_points[-2].mirrored_position, builder_points[-1].mirrored_position, v.mirrored_position]
		
		# Update the mesh
		surfaces[m].clear()
		surfaces[m].begin(Mesh.PRIMITIVE_TRIANGLES)
		
		for point in points:
			surfaces[m].add_vertex(point)
		
		surfaces[m].commit(meshes[m].get_mesh())
		meshes[m].set_mesh(surfaces[m].commit())


func on_color_changed(color, mesh_enum):
	meshes[mesh_enum].material_override.albedo_color = color


func on_material_select(mesh_enum):
	active_material = mesh_enum
