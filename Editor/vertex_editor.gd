class_name VertexEditor extends Node3D

enum Planes { X_PLANE, Y_PLANE, Z_PLANE, NONE }
enum Mode { PLACE, MODIFY }

const vertex := preload("res://Editor/vertex.tscn")
signal point_moved(Vertex)
signal point_clicked(Vertex)
signal point_hidden
signal point_removed
signal action_cancelled
var Log: Logger

# Plane Objects
@onready var x_plane = $X_Plane
@onready var y_plane = $Y_Plane
@onready var z_plane = $Z_Plane

# Export Settings
@export var plane_movement_speed = 1.0  # Movement speed of the plane
@export var mirror := true
@export var place_mode := Mode.PLACE:
	set(value):
		place_mode = value
		if place_mode == Mode.MODIFY:
			hide_planes()
		else:
			_update_selected_plane()
# Active plane management
@export var selected_plane: Planes = Planes.X_PLANE:
	set(value):
		selected_plane = value
		_update_selected_plane()
var active_plane: Node3D # Currently selected plane
var plane_direction := Vector3.MODEL_LEFT
var other_planes = [] # Planes that are not currently selected

# Raycasting
var camera: Camera3D

# Vertex Management
var new_point: Vertex
var hovered_point: Vertex
var active_point: Vertex
var placed_points: Array[Vertex] = []


func _ready():
	Log = Print.get_logger("Whisk", true)
	camera = get_viewport().get_camera_3d()
	selected_plane = Planes.X_PLANE  # Initialize the selected plane
	_create_new_following_point()  # Create the first following point


func _input(event):
	if event is InputEventMouseButton and get_viewport().get_mouse_position().x > (get_viewport().size.x * 0.875):
		return
	
	if (event.is_action("scroll_up") or event.is_action("scroll_down")) and !Input.is_key_pressed(KEY_SHIFT):
		var dir = Input.get_axis("scroll_down", "scroll_up")
		_move_active_plane(dir)
	
	elif event is InputEventMouseButton:
		if event.button_index == MOUSE_BUTTON_RIGHT and event.pressed and hovered_point:
			placed_points.remove_at(placed_points.find(hovered_point))
			Log.debug("Removed point at " + str(hovered_point.position))
			hovered_point.delete()
			hovered_point = null
			emit_signal("point_removed")
		elif place_mode == Mode.PLACE:
			if event.button_index == MOUSE_BUTTON_LEFT and event.pressed:
				if hovered_point != null:
					Log.debug("Connected existing point at " + str(hovered_point.position))
					emit_signal("point_clicked", hovered_point)
					hovered_point.activate()
				elif new_point.visible:
					emit_signal("point_clicked", new_point)
					_place_point_on_plane()
			elif event.button_index == MOUSE_BUTTON_RIGHT and event.pressed:
				deselect_all()
				emit_signal("action_cancelled")
		elif place_mode == Mode.MODIFY:
			if event.button_index == MOUSE_BUTTON_LEFT and event.pressed:
				if hovered_point:
					active_point = hovered_point
					hovered_point = null
					active_point.start_drag()
					snap_all_planes(active_point.position)
					_update_selected_plane()
				elif active_point:
					Log.debug("Point relocated to " + str(active_point.position))
					active_point.place()
					active_point = null
					hide_planes()
			elif event.button_index == MOUSE_BUTTON_RIGHT and event.pressed and active_point:
				active_point.cancel_drag()
				active_point = null
				emit_signal("action_cancelled")
	elif event.is_action("escape") and place_mode == Mode.MODIFY and active_point:
		active_point.cancel_drag()
		active_point = null
		emit_signal("action_cancelled")


## Update the position of the point as it collides with the plane.
func _process(_delta):
	if not new_point:
		return

	# Calculate the ray's origin and end points
	var from = camera.project_ray_origin(get_viewport().get_mouse_position())
	var to = from + camera.project_ray_normal(get_viewport().get_mouse_position()) * 1000

	# Set up ray query parameters
	var ray_query = PhysicsRayQueryParameters3D.new()
	ray_query.from = from
	ray_query.to = to
	if place_mode == Mode.PLACE:
		# Placing points, collide with points and planes.
		ray_query.collision_mask = 3
	elif active_point:
		# We are moving a point. Collide with plane only.
		ray_query.collision_mask = 1
	else:
		# We are looking for a point to modify. Collide with points only.
		ray_query.collision_mask = 2
	
	ray_query.exclude = other_planes + [new_point]
	if active_point:
		ray_query.exclude += [active_point]

	# Perform the raycast
	var space_state = get_world_3d().direct_space_state
	var intersection = space_state.intersect_ray(ray_query)
	
	if intersection:
		var p = intersection.collider
		if p is Vertex:
			if p != hovered_point and !p.is_queued_for_deletion():
				_try_exit_hover()
				Log.verbose("Start hover")
				new_point.hide()
				hovered_point = p
				emit_signal("point_moved", hovered_point)
				p.on_hover()
		else:
			_try_exit_hover()
			var point_position = intersection.position.round()  # Snap to nearest integer
			if place_mode == Mode.PLACE:
				new_point.show()
				new_point.position = point_position
				emit_signal("point_moved", new_point)
			if place_mode == Mode.MODIFY:
				active_point.position = point_position
				emit_signal("point_moved", active_point)
	else:
		_try_exit_hover()
		if new_point.visible:
			new_point.hide()
			emit_signal("point_hidden")

## Move the active plane along its axis
func _move_active_plane(dir):
	if active_plane:
		active_plane.position += plane_direction * dir
		if active_plane.position.length() > 50:
			active_plane.position = active_plane.position.normalized() * 50


func snap_all_planes(pos: Vector3):
	x_plane.position.x = pos.x
	y_plane.position.y = pos.y
	z_plane.position.z = pos.z


## Deactivate all currently hightlighted points.
func deselect_all():
	for p in placed_points:
		p.deactivate()


func _try_exit_hover():
	if hovered_point:
		Log.verbose("Exit hover")
		emit_signal("point_hidden")
		hovered_point.exit_hover()
		hovered_point = null


## Hide all three planes without clearing selected_plane
func hide_planes():
	Log.verbose("Hid all planes")
	other_planes = [x_plane, y_plane, z_plane]
	for plane in other_planes:
		plane.visible = false


func _update_selected_plane():
	match selected_plane:
		Planes.X_PLANE:
			active_plane = x_plane
			plane_direction = Vector3.MODEL_LEFT
			other_planes = [y_plane, z_plane]
		Planes.Y_PLANE:
			active_plane = y_plane
			plane_direction = Vector3.MODEL_TOP
			other_planes = [x_plane, z_plane]
		Planes.Z_PLANE:
			active_plane = z_plane
			plane_direction = Vector3.MODEL_FRONT
			other_planes = [x_plane, y_plane]
		Planes.NONE:
			active_plane = null
			other_planes = [x_plane, y_plane, z_plane]
	
	# Update plane visibility
	for plane in [x_plane, y_plane, z_plane]:
		plane.visible = plane == active_plane
	
	if active_plane:
		Log.verbose("Selected plane: " + str(active_plane.name))
	else:
		Log.verbose("No plane selected.")


func _create_new_following_point():
	# Create a new point.
	new_point = vertex.instantiate()
	add_child(new_point)


func _place_point_on_plane():
	new_point.place()
	new_point.activate()
	placed_points.append(new_point)
	Log.debug("Placed point at " + str(new_point.position))
	_create_new_following_point()
