class_name Vertex extends StaticBody3D

@onready var not_placed = $"Not Placed"
@onready var placed = $Placed
@onready var active = $Active
@onready var highlight = $Highlight
@onready var edit = $Edit
var used := false
var last_pos: Vector3
var selected: bool:
	get:
		return active.visible or edit.visible
var grid_position: Vector3:
	get:
		return global_position
var mirrored_position: Vector3:
	get:
		return Vector3(global_position.x, global_position.y, -global_position.z)


## Set this to a floating vertex.
func _ready():
	make_invisible()
	not_placed.visible = true
	collision_layer = 0


## Place this vertex in the world, turning it on.
func place():
	make_invisible()
	collision_layer = 2
	last_pos = position
	placed.visible = true


## Color this vertex while it is being used.
func activate():
	make_invisible()
	active.visible = true


## Start moving this point in 3D space. End the drag by calling place()
func start_drag():
	make_invisible()
	edit.visible = true


## Cancel moving this point. Reset the position to the start of the drag.
func cancel_drag():
	position = last_pos
	make_invisible()
	placed.visible = true


## Stop coloring this vertex.
func deactivate():
	make_invisible()
	placed.visible = true


## Mouse over this node. Disabled if this node is already active.
func on_hover():
	if !active.visible:
		highlight.visible = true


## Stop mousing over this node.
func exit_hover():
	highlight.visible = false


## Remove this vertex.
func delete():
	queue_free()


## Turn everything off.
func make_invisible():
	not_placed.visible = false
	placed.visible = false
	active.visible = false
	highlight.visible = false
	edit.visible = false
