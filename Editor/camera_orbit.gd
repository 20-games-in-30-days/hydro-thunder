extends Node3D
@onready var camera_3d = $Camera3D
@export var camera_limits := Vector2(20, 150)

# Sensitivity of the mouse movement
@export var sensitivity = 0.05
@export var zoom_speed = 1.0

# Current rotation angles
var yaw = 0.0
var pitch = 0.0

func _ready():
	# Hide the cursor and capture it when the middle mouse button is held down
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)

func _process(_delta):
	if Input.is_mouse_button_pressed(MOUSE_BUTTON_MIDDLE):
		# Capture the mouse movement
		var mouse_movement = get_viewport().get_mouse_position() - get_viewport().size * 0.5

		# Calculate the new rotation angles
		yaw -= mouse_movement.x * sensitivity
		pitch -= mouse_movement.y * sensitivity
		pitch = clamp(pitch, -80, 80)  # Limit the pitch to avoid gimbal lock

		# Apply the rotation to the Node
		rotation_degrees = Vector3(pitch, yaw, 0)

		# Reset the mouse position to the center of the viewport
		get_viewport().warp_mouse(get_viewport().size * 0.5)
	else:
		# Release the cursor when the middle mouse button is released
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	
	# Camera movement when Shift is held and mouse wheel is scrolled
	# Keep slightly off center to help with placement.
	if Input.is_key_pressed(KEY_SHIFT):
		if Input.is_action_just_pressed("scroll_down"):
			camera_3d.position.z = clamp(camera_3d.position.z + zoom_speed, camera_limits.x, camera_limits.y)
			camera_3d.position.x = camera_3d.position.z / 20.0
		elif Input.is_action_just_pressed("scroll_up"):
			camera_3d.position.z = clamp(camera_3d.position.z - zoom_speed, camera_limits.x, camera_limits.y)
			camera_3d.position.x = camera_3d.position.z / 20.0
